package com.nec.did.i18n;

import java.util.ArrayList;
import java.util.Arrays;

public class RuleError implements java.io.Serializable {
    
    static final long serialVersionUID = 1L;

    private ErrorCodes errorCode;
    private ArrayList<Object> args;

    RuleError() {}

    public RuleError(ErrorCodes code, Object... args) {
        this.errorCode = code;
        this.args = new ArrayList<>();
        Arrays.stream(args).forEach(arg -> {
            this.args.add(arg);
        });
    }

    public ArrayList<Object> getArgs() {
        return args;
    }

    public ErrorCodes getErrorCode() {
        return errorCode;
    }

    public void setArgs(ArrayList<Object> args) {
        this.args = args;
    }

    public void setErrorCode(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
