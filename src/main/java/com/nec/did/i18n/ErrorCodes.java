package com.nec.did.i18n;

public enum ErrorCodes {
    InvalidRange("error_invalidRange"),
    InvalidSymbol("error_invalidSymbol"),
    MissingField("error_missingField"),
    MaxLengthExceeded("error_maxLengthExceeded");

    private String strSlug;

    ErrorCodes(String numVal) {
        this.strSlug = numVal;
    }

    public String getStrSlug() {
        return strSlug;
    }
}
