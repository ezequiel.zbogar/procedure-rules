package com.nec.did.entity;

import java.util.HashMap;

public class Procedure implements java.io.Serializable {
    
    static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;
    private String typeUuid;
    private Integer severity;
    private HashMap<String, Object> extraData;
    
    public Procedure() {
    }

    public Procedure(Long id, String name, String description, String typeUuid, Integer severity, HashMap extraData) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.typeUuid = typeUuid;
        this.severity = severity;
        this.extraData = extraData;
    }

    public Long getId() {
        return id;
    }

    public Integer getSeverity() {
        return severity;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getTypeUuid() {
        return typeUuid;
    }

    public HashMap<String, Object> getExtraData() {
        return extraData;
    }

    public void setExtraData(HashMap<String, Object> extraData) {
        this.extraData = extraData;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    public void setTypeUuid(String typeUuid) {
        this.typeUuid = typeUuid;
    }
}
